import codecs
import random
import math

def encrypt(mensaje, clave_p, m):
    k = random.randint(0, 2**7)
    ke = (k**clave_p) % m

    c = ""
    for i in mensaje:
        c += chr(ord(i)^k)

    ke_hex = "%02x" % ke
    if len(ke_hex) % 2:
        ke_hex = "0" + ke_hex

    cm = "%s::%s" % (ke_hex, codecs.encode(c.encode(), 'hex').decode())

    return cm

def decrypt(mensaje, clave_s, m):

    ke_hex, c_hex = mensaje.split('::')

    ke = int.from_bytes(codecs.decode(ke_hex, 'hex'), byteorder='big')
    k = (ke**clave_s) % m

    c = codecs.decode(c_hex, 'hex')

    mensaje = ""
    for i in c:
        mensaje += chr(i^k)

    return mensaje


def is_prime(i):
    for j in range(int(math.sqrt(i)))[2:]:
        if i%j == 0:
            return False
    return True

def gen_key(e):
    p = random.randint(100, 500)
    while not is_prime(p):
        p += 1

    q = random.randint(100, 500)
    while not is_prime(q):
        q += 1
    
    n = p*q

    fi = (p-1)*(q-1)

    for d in range(n):
        if (d*e) % fi == 1:
            return dict(p=e, s=d, m=n)


